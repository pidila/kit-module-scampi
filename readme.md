# Kit de création d'un module pour Scampi

Procédure pour le PiDILA
------------------------

### Pendant le dev

* Répertoire du module sur une branche dédiée de Scampi
* Fichiers twig dans le dépôt pidila (branche dédiée)
    - checkout du submodule sur la branche dédiée de scampi
    - import du scss dans _assets/project/scss/partials/_scampi-demo.scss
    - $ gulp prep:js-scampi && gulp dev
    - la page est visible sur /scampi/documentation/mon_module

### Après dev et revue, quand tout est bon

* Scampi : merge branche mon_module sur branche **develop**
* Pidila : merge branche mon_module sur branche **dev**
* copie de 4-mon-module.twig dans dev/templates/inc/styleguide dans le dépôt scampi-twig, branche **dev**)
* Update et push du submodule scampi sur pidila et scampi-twig

Procédure pour un contributeur externe
--------------------------------------

* Forker le dépôt Scampi.
* Créer une branche mon_module.
* Préparer le module dans mon_module sans s'occuper des deux fichiers twig que l'équipe du PiDILA prendra en charge.
* Penser à vérifier la conformité RGAA.
* Faire un pull request.
