# Mon module

Présentation
-------------

Description du module.

### Accessibilité

Points d'attention particuliers si nécessaire.

Utilisation
-----------

Comment le mettre en œuvre.

### Script 

(si nécessaire)

Exemple
-------

``` html

pattern html du module

```
